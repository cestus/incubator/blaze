module code.cestus.io/blaze

go 1.16

require (
	github.com/felixge/httpsnoop v1.0.1
	github.com/go-chi/chi/v5 v5.0.3
	github.com/go-logr/logr v0.4.0
	github.com/magicmoose/zapr v0.2.0
	github.com/onsi/ginkgo/v2 v2.1.3
	github.com/onsi/gomega v1.17.0
	go.opentelemetry.io/contrib v0.20.0
	go.opentelemetry.io/contrib/instrumentation/net/http/httptrace/otelhttptrace v0.20.0
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.20.0
	go.opentelemetry.io/otel v0.20.0
	go.opentelemetry.io/otel/trace v0.20.0
	go.uber.org/zap v1.16.0
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
